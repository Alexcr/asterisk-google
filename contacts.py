#!/usr/bin/python
import atom,re,sys
import gdata.contacts
import gdata.contacts.service

def PhoneInFeed(feed,nphone):
  for i, entry in enumerate(feed.entry):
    for phone in entry.phone_number:
      if re.findall(nphone,phone.text):
        return entry.title.text
  return False

def main():
        email = "alexcr.telecom@gmail.com"
        password = "super-puper-password"
        phone = sys.argv[1]
        name = None
        #here I check that the number is more than 4 digits
        #otherwise it is to simple and will match too much
        if phone.__len__() > 4:
                #and here i'd like to remove leading zeroes, pluses and country codes
                #so if you have two contacts with the same numbers but in different areas
                #then you will get what you ask for :)
                phone = re.sub('^[0+]+[0-9]{2}', '', phone)
                gd_client = gdata.contacts.service.ContactsService()
                gd_client.email = email
                gd_client.password = password
                gd_client.source = 'gcontact2ast'
                gd_client.ProgrammaticLogin()
                query = gdata.contacts.service.ContactsQuery()
                #oh, yeah I know, and don't care to get 1k of results (189 here and works)
                query.max_results = 1000

                feed = gd_client.GetContactsFeed(query.ToUri())
                name = PhoneInFeed(feed,phone)
        if not name:
                #here is how I call the unknown numbers calling in
                name = "Caller"
        sys.stdout.write(name)

if __name__ == "__main__":
        main()